# At this point, you have not defined sum() yet
# so python uses its built-in sum() function, which expects
# one "iterable" parameter (i.e., a list) and sums over it
ergebnis= sum([31,11])
# Use string formatting [http://www.learnpython.org/en/String_Formatting] for print
print 'built-in sum([31,11])=%d' % ergebnis # Use Over, not Step to step over print in the debugger.

print 'Redefining sum() function'
def sum(a,b):
    '''
    a function that takes two parameters as innput and returns their sum
    '''
    c= a+b
    return c


# Now that you have defined your own sum() function
# (if you pay careful attention to the globals,
# you'll see an entry for the sum function that you defined yourself)
ergebnis= sum(31,11)
pass # look at the list of globals: c is unknown, but you have an ergebnis
c= ergebnis
pass # now c is defined not as local, but as global.
print 'sum(31,11)=%d' % c # use Over, not Step

# Now you define a function that really puts the debugger to work
# This function uses recursion, i.e., it calls itself with different parameters
# The stack keeps track of the sequence of function calls
# and the local values of the variables m, e, and p.
def power(m, e):
    '''
    a function that computes the e[xponent]-th power of m[antissa] for any integer e
    '''
    if e > 0: # m**e = m * m**(e-1)
        p= m*power(m, e-1)
    elif e < 0: # m**(-e) = 1/(m**e)
        p= 1.0/power(m, -e)
    else: # e==0 => m**0 = 1
        p= 1
    return p # in this line, pay attention to the locals and the stack

print 'Define square(ruth)=power(ruth,2)'
# Calculate square of root as second power of ruth
# Note the function definition in the list of globals
def square(ruth):
    return power(ruth, 2)

c= power(4,2)
print 'power(4,2)=%d' % c # use Over, not Step
c= power(2,3)
print 'power(2,3)=%d' % c # use Over, not Step
c= power(2,-2)
print 'power(2,-2)=%d' % c # use Over, not Step
c= square(7)
print 'square(7)=%d' % c # use Over, not Step

print 'Redefine square(ruth) as ruth*ruth'
def square(ruth):
    return ruth*ruth

c= square(7)
print 'square(7)=%d' % c # use Over, not Step

print 'Define iterative square root function'
def wurzelRek(a, x, epsilon=.00001):
    """
    recursive root function
    """
    y= (x+a/x)/2
    if abs(y-x)<epsilon:
        return x
    else:
        return wurzelRek(a, y, epsilon)

def wurzel(a, epsilon=.00001):
    """
    root function starter
    """    
    return wurzelRek(a, a*1.0, epsilon)

for q in range(1,20):
    c= wurzel(q)
    print 'wurzel(%f)=%f' % (q,c) # use Over, not Step

