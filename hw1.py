# It generally helps to monitor the results of each line, step by step
# A good possibility to do this is the idle debugger
# First, idle needs to be opened
# such that both the shell and the file window are open.
#
# Once both windows are open, in the Python Shell window:
# Debug -> Debugger
# The Debug Control window should pop up
# In the file window:
# Run -> Run Module
# Control is now passed on to the Debug Control window.
# For now, the python script will be stepped through line by line
# with the Over button
# (Step button will go into functions like print, which will get confusing)
#
# In the Locals partition of the Debug Control window,
# variables and their values will show up once they are assigned.

# An equivalent way to monitor the variables is to print them immediately
# but once you have learned how to use the debugger,
# which is a very useful skill to have anyhow,
# you don't need to insert the print statements
# Here, I'm putting them in.
marker = "AFK"
print marker
replacement = "away from keyboard"
print replacement
line = "Ich bin SCHLAFF. I will now go to sleep and be AFK until lunch time tomorrow."
print line
mark =  line .find (marker)
print mark
markerlength = len(marker)
print markerlength
after = mark + markerlength
print after
replaced = line [:mark] + replacement + line [after:]



middle= line .find(marker[:-1])
mark= len (marker)
beginning= line [:middle]
cut = line [middle:]
end = cut [mark:]
replacement= beginning+ replacement+end
print replacement
print replaced


