# Programm zum Wurzelziehen
#
# Aufruf:
# python wurzel.py
#

# Bei single step in idle Debugger:
# Fuer alles ausser print: benutze Step
# Fuer print Zeilen: benutze Over, oder Out, nachdem idle Debugger anderswo hin springt
# Dieses Skript protokolliert aber auch ohne Debugger, was passiert

protokoll=True  # Boolesche Variable: Protokolliere den Ablauf falls True
if protokoll:
    indent=''   # String zur Einrueckung von Kommentaren, abhaengig von Rekursionstiefe
#
# Wenn du das eigentliche Programm verstehen willst, 
# kannst du alle 
# if protokoll: 
# Bloecke ignorieren oder sogar loeschen.
#

print 'Define iterative square root function'
def wurzelRek(a, x, epsilon=.00001):
    """
    Rekursive Wurzelfunktion
    Parameter:
    a: Zahl, von der die Wurzel gezogen werden soll
    x: Schaetzwert fuer die Wurzel
    epsilon: geforderte Genauigkeit (optionaler Parameter, voreingestellt auf 10^-5)
    """
    y= (x+a/x)/2    # errechne neuen Schaetzwert fuer Wurzel

    if protokoll:
        global indent       # mache globale Variable indent zugaenglich
        indent= indent+'  ' # ruecke Protokoll um 2 Stellen nach rechts
        print '%swurzelRek(%f, %f, %f)' %(indent, a, x, epsilon)    # protokolliere Funktionsaufruf mit Parametern
        print '%s  y=%f' % (indent, y)  # protokolliere neuen Schaetzwert fuer Wurzel

    if abs(y-x)<epsilon:    # Betrag der Differenz zwischen neuer und alter Schaetzung kleiner als geforderte Genauigkeit?
        result= x   # falls ja, gib die alte Schaetzung zurueck. Die ist gut genug.
    else:
        result= wurzelRek(a, y, epsilon)    # sonst berechne rekursiv einen verbesserten Schaetzwert

    if protokoll:
        print '%swurzelRek(%f, %f, %f) returns %f' %(indent, a, x, epsilon, result) # protokolliere Funktionsergebnis
        indent= indent[:-2] # Einrueckung um 2 Stellen nach links zurueckverschieben

    return result   # gib das Ergebnis zurueck

def wurzel(a, epsilon=.00001):
    """
    Funktion zur Berechnung einer Quadratwurzel
    Parameter:
    a: Zahl, von der die Wurzel gezogen werden soll
    epsilon: geforderte Genauigkeit (optionaler Parameter, voreingestellt auf 10^-5)
    """    
    if protokoll:
        global indent       # mache globale Variable indent zugaenglich
        indent= indent+'  ' # ruecke Protokoll um 2 Stellen nach rechts
        print '%swurzel(%f, %f)' %(indent, a, epsilon)  # protokolliere Funktionsaufruf mit Parametern

    result= wurzelRek(a, a*1.0, epsilon)    # starte rekursive Berechnung 
    # mit (float)a als erster Schaetzung, damit python keine Integerarithmetik benutzt

    if protokoll:
        print '%swurzel(%f, %f) returns %f' %(indent, a, epsilon, result)   # protokolliere Funktionsergebnis
        indent= indent[:-2] # Einrueckung um 2 Stellen nach links zurueckverschieben

    return result   # gib das Ergebnis zurueck

if '__main__' == __name__:  
    # Falls dieses Skript das Hauptprogramm ist und nicht von einem anderen aufgerufen wird

    # hier mache ich die Genauigkeit mit Absicht ungenauer, 
    # damit das Protokoll nicht ohne Aenderung genau das geforderte Ergebnis ausgibt, 
    # sondern nur dann, wenn du auch verstanden hast, was passiert.
    # also musst du entweder die default Einstellung fuer den optionalen Parameter verwenden, 
    # oder die Genauigkeit hier aendern.
    epsilon= .00001 

    # hier teste ich das Wurzelprogramm fuer mehrere Zahlen 
    # Die Liste so zu aendern, dass nur die Wurzel aus 9 berechnet wird,
    # ist zwar einfach, aber du kannst das Programm nicht einfach unveraendert laufen lassen.
    for q in range(9,10):    # fuer q von einschliesslich 9 bis ausschliesslich 10

        if protokoll:
            print '%scalling wurzel(%f, %f)' %(indent, q, epsilon)  # protokolliere Funktionsaufruf mit Parametern

        c= wurzel(q, epsilon)   # rufe Wurzelfunktion auf
        print 'wurzel(%f)=%f' % (q,c) # ob protokoll oder nicht, gib das Ergebnis aus.

